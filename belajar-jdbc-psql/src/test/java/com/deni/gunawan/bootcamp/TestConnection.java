/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deni.gunawan.bootcamp;

import junit.framework.TestCase;
import lombok.extern.slf4j.*;
import org.junit.Test;
import com.deni.gunawan.bootcamp.DatasourceConfig;
    
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
    
/**
 *
 * @author chocolate
 */
@Slf4j
public class TestConnection extends TestCase{
    
    private DatasourceConfig config;
    
    @Override
    protected void setUp() throws Exception{
        this.config = new DatasourceConfig();
    }
    
    
   @Test 
    public void testConnectionToDB() throws  SQLException{
        DataSource dataSource = this.config.getDataSource();
        Connection connection = dataSource.getConnection();
    }
}
        