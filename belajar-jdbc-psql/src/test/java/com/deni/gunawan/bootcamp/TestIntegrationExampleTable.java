/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deni.gunawan.bootcamp;

import com.deni.gunawan.bootcamp.dao.ExampleTableDao;
import com.deni.gunawan.bootcamp.entity.ExampleTable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.sql.DataSource;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 *
 * @author chocolate
 */
@Slf4j
public class TestIntegrationExampleTable extends TestCase{
     
     private DataSource dataSource;

    private ExampleTableDao dao;

    @Override
    protected void setUp() throws Exception {
        this.dataSource = new DatasourceConfig().getDataSource();
    }

    @Test
    public void testFindById() {
        try (Connection connection = this.dataSource.getConnection()) {
            this.dao = new ExampleTableDao(connection);

            Optional<ExampleTable> existId = this.dao.findById("001");
            assertTrue("find by id 001 data exist", existId.isPresent());
            ExampleTable id001 = existId.get();
            assertEquals("name with id 001 is Dimas Maryanto actualy",
                    id001.getName(),
                    "Dimas Maryanto");

            Optional<ExampleTable> notExistId = this.dao.findById("006");
            assertFalse("find by id 006 data not exist actualy", notExistId.isPresent());
        } catch (SQLException ex) {
            log.error("can't fetch data", ex);
        }
    }

    @Test
    public void testPagingList() {
        try (Connection connection = this.dataSource.getConnection()) {
            this.dao = new ExampleTableDao(connection);

            List<ExampleTable> showMoreThanStored = this.dao.findAll(0l, 10l, null, null, null);
            assertEquals("jumlah data example table when limit 10 and offset 0", showMoreThanStored.size(), 5);

            List<ExampleTable> showOnly3 = this.dao.findAll(0l, 3l, null, null, null);
            assertEquals("jumlah data example table when limit 3 and offset 0", showOnly3.size(), 3);

            List<ExampleTable> showOnly2 = this.dao.findAll(3l, 3l, null, null, null);
            assertEquals("jumlah data example table when limit 3 and offset 3", showOnly2.size(), 2);
        } catch (SQLException ex) {
            log.error("can't fetch data", ex);
        }
    }

    @Test
    public void testSavingData() {
        try (Connection connection = this.dataSource.getConnection()) {
            this.dao = new ExampleTableDao(connection);
            ExampleTable returnValue = this.dao.save(
                    new ExampleTable(
                            null,
                            "Muhamad Purwadi",
                            Date.valueOf(LocalDate.now()),
                            Timestamp.valueOf(LocalDateTime.now()),
                            true,
                            0l, new
                            BigDecimal(100000),
                            "test data",
                            0f)
            );
            assertNotNull("employee id not null", returnValue.getId());
            log.info("employee: {}", returnValue);

            this.dao.removeById(returnValue.getId());

            List<ExampleTable> list = this.dao.findAll();
            assertEquals("jumlah data example table", list.size(), 5);
        } catch (SQLException ex) {
            log.error("can't fetch data", ex);
        }
    }
    
    
}
