/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deni.gunawan.bootcamp.dao;

import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author chocolate
 */
public interface CrudRepository<T, ID> {
    
   public T save(T value) throws Exception;
   
   public T update(T value) throws Exception;
   
    public Boolean removeById(ID value) throws SQLException;

    public Optional<T> findById(ID value) throws SQLException;

  
    public List<T> findAll() throws SQLException;

    public List<T> findAll(Long start, Long limit, Long orderIndex, String orderDirection, T param) throws SQLException;
}

   
