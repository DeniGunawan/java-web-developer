/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deni.gunawan.bootcamp;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author chocolate
 */
@Slf4j
public class DatasourceConfig {
   
    private final HikariConfig config;
   
    public DatasourceConfig(){
        
        Properties prop = new Properties();
        InputStream input = DatasourceConfig.class.getResourceAsStream("/application.properties");
        try {
            prop.load(input);
            input.close();
        } catch (IOException e) {
            log.error("can't load file propertie", e);
        }
        this.config = new HikariConfig();
        config.setJdbcUrl(prop.getProperty("jdbc.url"));
        config.setUsername(prop.getProperty("jdbc.username"));
        config.setPassword(prop.getProperty("jdbc.password"));
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    }

 
    
    public DataSource getDataSource(){
        return new HikariDataSource(config);
    }
}
