/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deni.gunawan.bootcamp.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author chocolate
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExampleTable {
    private String id;
    private String name;
    private Date createdDate;
    private Timestamp createdTime;
    private Boolean active;
    private Long counter;
    private BigDecimal currency;
    private String description;
    private Float floating;
}
