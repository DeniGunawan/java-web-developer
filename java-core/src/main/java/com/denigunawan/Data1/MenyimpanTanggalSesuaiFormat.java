package com.denigunawan.Data1;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MenyimpanTanggalSesuaiFormat {
    public static void main(String[] args) {
        Date tanggalSekarang = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy hh:mm:ss z");
        System.out.println("Tanggal Sekarang " + format.format(tanggalSekarang));
    }
}
