package com.denigunawan.FlowControl2.Percabangan.SwitchLebihDalam;

public class Switch2 {
    public static void main(String[] args) {
        Integer aku = 10;

        switch (aku){
            case 10:
                System.out.println("aku bernilai 10");
            case 11:
                System.out.println("aku bernilai 11 loh");
            case 12:
                System.out.println("aku bernilai 12 loh");
                break;
            default:
                System.out.println("nilai yang kamu cari tidak ada ");
        }
    }
}
