package com.denigunawan.FlowControl2.Percabangan.IFlebihDalam;

public class IfElse {
    public static void main(String[] args) {
        Integer aku = 10;

        if(aku > 10){
            System.out.println("Aku lebih dari 10");
        }else{
            System.out.println("opss sayang, aku tidak lebih dari 10");
        }
        // perintah terakhir dijalankan karna kondisi di atas tidak benar atau false
    }
}
