package com.denigunawan.FlowControl2.Percabangan;

public class Switch {
    public static void main(String[] args) {


        Integer nilaiIniAdalah = 10;

        switch (nilaiIniAdalah){
            case 10:
                System.out.println("nilai akhirnya adalah 10 ");
//                break;
            case 5:
                System.out.println("nilai akhirnya adalah 5");
                break;
            default:
                System.out.println("nilai yang dicari tidak ada ");
        }
    }
}
