package com.denigunawan.FlowControl2.Percabangan.IFlebihDalam;

public class If {
    public static void main(String[] args) {
        Integer aku = 10;


        if( aku > 10){
            System.out.println("aku lebih dari 10");
        }

        // kita tidak menampilkan apa apa , karna kondisi pertama salah dia akan menjalankan kondisi kedua
    }
}
