package com.denigunawan.FlowControl2.Percabangan.IFlebihDalam;

public class IfElseIf {
    public static void main(String[] args) {
        Integer aku = 10;

        if(aku > 10) {
            System.out.println("aku lebih dari 10 ");
        }else if(aku > 5){
            System.out.println("aku lebih dari 5 loh");
        }else{
            System.out.println("aku tidak tahu ");
        }
    }
}
