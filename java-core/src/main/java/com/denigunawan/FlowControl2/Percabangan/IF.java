package com.denigunawan.FlowControl2.Percabangan;

public class IF {
    public static void main(String[] args) {

        int hargaBakso = 20000;


        if(hargaBakso == 1000){
            System.out.println("Betul Harganya sama dengan 1000");
        }else if(hargaBakso == 20000){
            System.out.println("Betul Harganya sama dengan 200000");
        }else{
            System.out.println("Harganya tidak sama ");
        }
        /*
        *   model pertama
        *
        * if (hargaBakso == 1000){
        *   System.out.println("betul harganya sama dengan 1000);
        * }
        *  karna kondisi tidak benar , maka nanti akan menghasilkan nilai null(kosong)
        * */


        /*
        *       model kedua
        *
        * id(hargaBakso == 1000){
        *   System.out.println("betul harganya sama dengan 1000);
        * }else {
        *   System.out.println("Harga bakso tidak sama );
        * }
        * karna kondisi pertama tidak dipenuhi atau tidak benar , maka
        * akan dijalankan kondisi kedua yaitu akan menampilkan baris tulisan didalam else
        *
        * */


        /*
        *       model ketiga
        *
        * if(hargaBakso == 1000){
        *   System.out.println("betul harganya sama dengan 1000);
        * }else if(hargaBakso == 20000){
        *   System.out.println("harga bakso sama dengan 20000")
        * }else {
        *   System.out.println("Harga bakso tidak sama")
        * }
        *
        *karna kondisi pertama salah , maka akan dijalankan kondisi  kedua , karna controlnya benar
        * dan kondisi ketiga tidak jalan, karna kondisi pertama sudah benar
        *
        *
        * */
    }
}
