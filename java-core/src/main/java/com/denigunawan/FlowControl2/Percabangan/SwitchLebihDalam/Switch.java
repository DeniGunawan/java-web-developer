package com.denigunawan.FlowControl2.Percabangan.SwitchLebihDalam;

public class Switch {
    public static void main(String[] args) {
        Integer aku  = 10;

        switch (aku){
            case 2:
                System.out.println("nilai aku 2");
                break;
            case 3:
                System.out.println("nilai aku 3");
                break;
            case 10:
                System.out.println("nilai aku 10");
                break;
            case 11:
                System.out.println("nilai aku 11");
                break;
            default:
                System.out.println("nilai tidak ada");
        }
    }
}
