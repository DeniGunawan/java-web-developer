package com.denigunawan.FlowControl2.TimeToPratice2;

public class ReverseStart {
    public static void main(String[] args) {
        Integer sisi = 5;
        for(int  i = sisi; i > 0; i--){
            for(int j = 0; j < i; j++){
                System.out.print(String.format("%d", (i-j)));
            }
            System.out.println();
        }
    }
}
