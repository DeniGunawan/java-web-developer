package com.denigunawan.FlowControl2.TimeToPratice2;

public class KarakterBintangGanji {
    public static void main(String[] args) {
        Integer sisi = 5;
        for(int i = 0; i < sisi; i++){
            for(Integer j = 0; j <= i; j++){
                if(i % 2 == 1 ) break;
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
