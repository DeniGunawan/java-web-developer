package com.denigunawan.FlowControl2.TimeToPratice2;

public class KarakterBintang {
    public static void main(String[] args) {

        Integer sisi = 5;

        for(int i = 0; i < sisi; i++){
            for(Integer j = 0; j <= i; j++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
