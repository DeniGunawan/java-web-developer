package com.denigunawan.FlowControl2.TimeToPratice2;

public class PengulanganGenap {
    public static void main(String[] args) {

        // cara pertama
//            int x;
//        for( x = 1; x < 101; x+=2) {
//            System.out.println(x);
//        }
        // kita deklarasikan variable dulu
        Integer sampai = 100;
        // kita buat perulangan
        for(Integer dari = 1; dari <= sampai; dari++){
            if(dari % 2 == 0){
                System.out.println(String.format("%x", dari));
            }
        }

        }
}

