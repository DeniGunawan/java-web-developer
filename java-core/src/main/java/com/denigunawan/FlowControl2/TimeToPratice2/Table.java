package com.denigunawan.FlowControl2.TimeToPratice2;

public class Table {
    public static void main(String[] args) {
        Integer sisi = 5;

        for(int i = 0; i <= sisi; i++){
            for(int j = 0; j <= sisi; j++){
                if(i % 2 == 0)
                System.out.print(String.format("%d ", i * j));
                else
                    System.out.print(String.format("%2.0f ", Math.pow(i, j)));
            }
            System.out.println();
        }
    }
}
