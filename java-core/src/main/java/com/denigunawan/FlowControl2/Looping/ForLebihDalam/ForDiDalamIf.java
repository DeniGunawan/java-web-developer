package com.denigunawan.FlowControl2.Looping.ForLebihDalam;

public class ForDiDalamIf {

    public static void main(String[] args) {
        boolean nilai =  true;

        if(nilai){
            for(int i = 0; i < 10; i++){
                System.out.println("sekarang index ke " + i);
            }
        }
    }
}
