package com.denigunawan.FlowControl2.Looping.ForLebihDalam;

public class ForReverseIndex {
    public static void main(String[] args) {
        for(int i = 10 ; i > 0; i--){
            System.out.println("Indexnya "+ i);
        }
    }
}
