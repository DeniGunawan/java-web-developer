package com.denigunawan.FlowControl2.Looping.ForLebihDalam;

public class ForDalamFor {
    public static void main(String[] args) {
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 2; j++){
                System.out.println("jalankan si j " + j);
            }
            System.out.println("jalankan si i " + i);
        }
    }
}
