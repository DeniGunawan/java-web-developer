package com.denigunawan.FlowControl2.Looping;

public class DoWhile {
    public static void main(String[] args) {
        Integer nilai = 0;

        do{
            System.out.println("saya semangat belajar java dasar yang penting ini " + nilai );
            nilai++;
        }while (nilai < 1001);
    }
}
