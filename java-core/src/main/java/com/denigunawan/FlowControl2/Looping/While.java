package com.denigunawan.FlowControl2.Looping;

public class While {
    public static void main(String[] args) {
        Integer i = 0;
        // while kondisinya kita check dahulu sebelum dijalankan
        // kita berikan control
        while(i < 100){
            // kemudian kita berikan action yang mau dijallankan
            System.out.println("jalankan sebanyak");
            // lalu kita increment atau ditambahkan
            i++;
        }

    }
}
