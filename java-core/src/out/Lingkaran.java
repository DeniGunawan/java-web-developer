package com.denigunawan;

public class Lingkaran {
    public static void main(String[] args) {
        java.lang.Integer jarijari = 15;
        java.lang.Integer diameter = jarijari * 2;
        java.lang.Double luas, phi=3.14;

        luas = phi*jarijari*diameter;

        System.out.println(
                "Nilai PI " + diameter +  ", maka kelilingnya adalah " + luas  );
    }

}
