package com.deni.trainingspringboot.PesertaDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.deni.trainingspringboot.dao.PesertaDao;
import com.deni.trainingspringboot.entity.Peserta;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
@Sql(
		executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
		scripts = "/data/peserta.sql"
)
public class PesertaDaoTest {

	@Autowired
	private PesertaDao test;

	@Autowired
	private DataSource ds;


	// menghapus
	@After
	public  void hapusData() throws Exception{
		String sql = "DELETE FROM peserta WHERE email = 'peserta@gmail.com'";
		try(Connection c = ds.getConnection()) {
			c.createStatement().executeUpdate(sql);
		}
	}


	// test untuk simpan data 
	@Test
	public void testInsert() throws SQLException {
		Peserta p = new Peserta();
		p.setNama("Peserta 001");
		p.setEmail("peserta@gmail.com");
		p.setTanggalLahir(new Date());
	
		test.save(p);
		
		String sql = "SELECT COUNT(*) AS jumlah FROM peserta WHERE email = 'peserta@gmail.com'";
	
		try (Connection c = ds.getConnection()){
		ResultSet rs = c.createStatement().executeQuery(sql);
		Assert.assertTrue(rs.next());

		Long jumlahRow = rs.getLong("Jumlah");
		Assert.assertEquals(1L , jumlahRow.longValue());
		}
	}

	// menghitung jumlah
	@Test
	public void testHitung(){
		Long jumlah = test.count();
		Assert.assertEquals(3L, jumlah.longValue());
	}
	// mencari by id
	@Test
	public void testCariById(){
		Peserta p = test.findOne("aa");
		Assert.assertNotNull(p);
		Assert.assertEquals("Peserta 001", p.getNama());
		Assert.assertEquals("test01@gmail.com", p.getEmail());

		Peserta px = test.findOne("xx");
		Assert.assertNull(px);
	}
}
