package com.deni.trainingspringboot.entity;

import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class Peserta {

    @Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")// menandakan ini adalahh primary class
    private String id;
    @Column(nullable = false) // tidak boleh kosong
    private String nama;
    @Column(nullable = false, unique = true) // tidak boleh kosong dan nama harus berbeda atau unique
    private String email;
    
    @Column(name = "tanggal_lahir", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date tanggalLahir;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	
	public void setTanggalLahir(Date tangggalLahir) {
		this.tanggalLahir = tangggalLahir;
	}
}
