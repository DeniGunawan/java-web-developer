package com.deni.trainingspringboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deni.trainingspringboot.entity.Peserta;

import java.util.Optional;

public interface PesertaDao extends JpaRepository<Peserta, String>{

    Optional<Peserta> findById (String id);

}
