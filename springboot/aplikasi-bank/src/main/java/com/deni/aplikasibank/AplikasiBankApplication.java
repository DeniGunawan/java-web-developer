package com.deni.aplikasibank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiBankApplication.class, args);
	}

}
