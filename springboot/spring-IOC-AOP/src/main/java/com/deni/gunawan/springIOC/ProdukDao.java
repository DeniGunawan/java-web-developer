package com.deni.gunawan.springIOC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProdukDao {

    @Autowired
    private KoneksiDatabase koneksi;

    public ProdukDao(){

    }
    public ProdukDao(KoneksiDatabase koneksi) {
        this.koneksi = koneksi;
    }

    public void setKoneksi(KoneksiDatabase koneksi) {
        this.koneksi = koneksi;
    }

    public void simpan(){
        System.out.println("Gunakan koneksi database" + koneksi.getNamaDatabase());
    }
}
