package com.deni.gunawan.springIOC;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDemo {
    public static void main(String[] args) {
        ApplicationContext ctx =  new ClassPathXmlApplicationContext("classpath:halo_spring.xml");
        KoneksiDatabase n = ctx.getBean(KoneksiDatabase.class);

        System.out.println("Server : " +n.getServer());
        System.out.println("Port : " +n.getPort());
        System.out.println("Database : " +n.getNamaDatabase());
        System.out.println("Username : " +n.getUsername());
        System.out.println("Password : " +n.getPassword());


        ProdukDao p = (ProdukDao) ctx.getBean("produkDao");
        p.simpan();
    }
}
