package com.deni.gunawan.springIOC;

public class NoSpringDemo {
    public static void main(String[] args) {
        KoneksiDatabase n = new KoneksiDatabase();
        n.setServer("localhost");
        n.setPort(1234);
        n.setNamaDatabase("ioc");
        n.setUsername("admin");
        n.setPassword("123");

        System.out.println("Server : " +n.getServer());
        System.out.println("Port : " +n.getPort());
        System.out.println("Database : " +n.getNamaDatabase());
        System.out.println("Username : " +n.getUsername());
        System.out.println("Password : " +n.getPassword());

        ProdukDao dao = new ProdukDao(n);
        dao.setKoneksi(n);
        dao.simpan();
    }
}
